#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##############################################################################
###                          Inv3-202Hg                                     ###
###     Inversion of the 202Hg to 198Hg isotopic ratios (d202Hg)           ###
###                Romain Brossier, Alain Manceau                          ###
###                          v1.0 07/2021                                  ###
##############################################################################


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.tri as tri
import matplotlib.patches as mpatches
from numpy.lib import recfunctions as rfn
import datetime


#linear inversion function
def solvelininvprob(G,Cdinv,dobs):
    A=G.T@Cdinv@G
    b=G.T@Cdinv@dobs
    mopt=np.linalg.solve(A,b)
    Cm=np.linalg.inv(G.T@Cdinv@G)
    return mopt,Cm

#linear fit function
def linearfit(x,y):
    A=np.zeros((2,2))
    A[0,0]=np.sum(x*x)
    A[0,1]=np.sum(x)
    A[1,0]=np.sum(x)
    A[1,1]=len(x)
    b=np.zeros((2,1))
    b[0,0]=np.sum(x*y)
    b[1,0]=np.sum(y)
    coef=np.linalg.solve(A,b)
    ybar=b[1,0]/len(y)
    ypred=coef[0,0]*x+coef[1,0]
    R2=np.sum((ypred-ybar)**2)/np.sum((y-ybar)**2)
    return coef,R2


###############################
# management of the output log
###############################
now = datetime.datetime.now()

print("")
print("***********************")
print("     Inv3-202Hg         ")
print("***********************")
print ("run launched : ")
print (now.strftime("%H:%M:%S %d/%m/%Y "))
print("")
print("Calculation of the species-specific d202Hg values by linear inversion")
print("")
print("Authors:")
print("Romain Brossier (romain.brossier@univ-grenoble-alpes.fr)")
print("Alain Manceau (alain.manceau@univ-grenoble-alpes.fr)")
print("")
print("Program Version : 1.0")
print("Release date : 15 July 2021")
print("")
print("The mathematical formalism can be found here:")
print("A. Manceau, R. Brossier, S. Janssen, T. Rosera, D. Krabbenhoft, Y. Cherel, P. Bustamante, B. Poulin, (2021) Mercury Isotope Fractionation by Internal Demethylation and Biomineralization Reactions in Seabirds: Implications for Environmental Mercury Science, Enviro. Sci. Technol., submitted) ")
print("The Python source can be found here:  => https://gricad-gitlab.univ-grenoble-alpes.fr/mercury_inversion/inversion_3species ")
print("")

#############################
# management of the inputs
#############################

print("***********************")
#input file name in csv format
file=str(input("input file name in csv format\n"))
print("file name is", file)
#print("assumed separator of csv in ;")
nheader=int(input("number of header lines in file\n"))
#nlines=int(input("number of lines of data to be read in file\n"))
#print("the assumed csv format is")
#print("header line 1")
#print(".....")
#print("header line n")
#print("sample ID1 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; Std %MeHg ; %Hg(Sec)4 ; Std %Hg(Sec)4 ; %HgSe ; Std %HgSe")
#print("sample ID2 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; Std %MeHg ; %Hg(Sec)4 ; Std %Hg(Sec)4 ; %HgSe ; Std %HgSe")
#print(".....")
#print("sample IDm ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; Std %MeHg ; %Hg(Sec)4 ; Std %Hg(Sec)4 ; %HgSe ; Std %HgSe")
#print("")
print("")
print("inversion management")
nrandom=1000
nrandom=int(input("number of random sampling for the uncertainty analysis (advised value is between 500 and 10000)\n"))




#############################
# READ INPUT FILE
#############################
#dfd=pd.read_csv(file,sep=';',header=None,nrows=nlines+nheader,comment='#')
dfd=pd.read_csv(file,sep=';',header=None,comment='#')
dfd=dfd.fillna(0)
#print(dfd)

#############################
# DO THE REAL WORK NOW
#############################
#index to start the data in the dfd pandas structure
istart=nheader
#extract the data part
dfd_data=np.array(dfd.loc[istart:,2:9].astype(float))

#fill the observation vector fill in with d202Hg
dobs=np.array([dfd_data[:,0]]).T
#print(dobs,np.shape(dobs))
#fill the standard deviation vector
stddobs=np.array([dfd_data[:,1]]).T
#print(stddobs,np.shape(stddobs))
#fill the G table now with input
G=0.01*np.concatenate((np.array([dfd_data[:,2]]).T,np.array([dfd_data[:,4]]).T,np.array([dfd_data[:,6]]).T),axis=1)
Gstd=0.01*np.concatenate((np.array([dfd_data[:,3]]).T,np.array([dfd_data[:,5]]).T,np.array([dfd_data[:,7]]).T),axis=1)

Cdinv=np.diagflat(stddobs**(-2))


#now solve the problem with linear inversion, taking into account only uncertainty on the d202Hg
mopt1,Cm1=solvelininvprob(G,Cdinv,dobs)

#now solve the problem with the random sampling approach to handle uncertainties on species fractions
mhist=np.zeros((3,nrandom))
A=np.zeros((3,nrandom))


#loop over random sampling
for i in range(0,nrandom):
    #start from the original G matrix
    Gt=G.copy()
    #now pertubate the G matrix with perturbation in the range of Gstd
    for k in range(0,len(G)):
        for kk in range(0,3):
            pert=Gstd[k,kk]*2*np.random.rand()-Gstd[k,kk]
            Gt[k,kk]=Gt[k,kk]+pert
    #now solve the problem with linear inversion with the perturbated G matrix
    mopt,Cm=solvelininvprob(Gt,Cdinv,dobs)
    #save the optimal solution in mhist
    mhist[:,i]=mopt[:,0]
    
#compute the average of the random sampling solutions
moptstat=np.array([np.sum(mhist,1)/nrandom]).T

#now compute the random-sampling-based covariance matrix
for i in range(0,nrandom):
    A[:,i]=mhist[:,i]-moptstat[:,0]
    
Cmens=A@A.T/(nrandom-1)
#extract also the diagonal to have the standard deviation
stdsolens=np.sqrt(np.diag(Cmens))




#compute the predicted d202Hg by the model  average model  
dcal=moptstat[0,0]*G[:,0]+moptstat[1,0]*G[:,1]+moptstat[2,0]*G[:,2]

#compute linear fit between calculated and experimental data
linfit,R2=linearfit(dobs[:,0],dcal)



#############################
# WRITE OUTPUTS 
#############################
print("")
now = datetime.datetime.now()      
print(" end of computation done ")
print (now.strftime("%H:%M:%S %d/%m/%Y "))
print("***********************")
print(" INPUTS")
print("***********************")
print("input file name",file)
print("number of random sampling",nrandom)
print("")
print("content of the input file read")
print(dfd.to_string())
#print(dfd)
print("")
print("***********************")
print(" OUTPUTS")
print("***********************")
print("")
#print("Optimal solution from linear inversion")
#print("solution for d202MeHg =",mopt[0,0])
#print("solution for d202Hg(Sec)4=",mopt[1,0])
#print("solution for d202HgSe=",mopt[2,0])
print("Optimal solution from linear inversion with random sampling")
print("solution for d202MeHg =",moptstat[0,0])
print("solution for d202Hg(Sec)4=",moptstat[1,0])
print("solution for d202HgSe=",moptstat[2,0])
print("standard deviation of d202MeHg from random sampling analysis=",stdsolens[0])
print("standard deviation of d202Hg(Sec)4 from random sampling analysis=",stdsolens[1])
print("standard deviation of d202HgSe from random sampling analysis=",stdsolens[2])
print("full random sampling covariance matrix, ordered as")
print("[[Variance d202MeHg              , Covariance d202MeHg-d202Hg(Sec)4, Covariance d202MeHg-d202HgSe    ]")
print("[Covariance d202MeHg-d202Hg(Sec)4, Variance d202Hg(Sec)4           , Covariance d202Hg(Sec)4-d202HgSe]")
print("[Covariance d202MeHg/d202HgSe    , Covariance d202Hg(Sec)4-d202HgSe, Variance d202HgSe               ]]")
print(Cmens)
header=[0] * nheader
tmp=np.around(dcal, 2)
header[-1]='d202Hg cal'
data_out=header+tmp.tolist()
dfd['10'] = data_out
pd.options.display.float_format = '{:,.2f}'.format #option for 2 digits printing
print("")
print("content of the input file + calculated d202Hg in the model")
print(dfd.to_string())
print("")
print("linear fit of the calculated versus measured species-averaged values of d202Hg, calculated without uncertainties")
print('y='+str(np.around(linfit[0,0],4))+'*x +'+str(np.around(linfit[1,0],4))+' with R2='+str(np.around(R2,4)))



print('end of output')


#############################
# OUTPUT 3D VIEW  
#############################

name=['%MeHg', '%Hg(Sec)4', '%HgSe']

b=np.array([[0.5,np.sqrt(3)/2]])
a=np.array([[0,0]])
c=np.array([[1,0]])
summits=np.concatenate((a,b,c,a),axis=0)
fig = plt.figure(1)
ax = fig.add_subplot(projection='3d')
#ax = fig.gca(projection='3d')

ax.set_xticklabels([])
ax.set_yticklabels([])
ax.zaxis.set_rotate_label(False)  # disable automatic rotation
ax.set_zlabel('$\u03B4^{202}$Hg(\u2030)',rotation=90)

Gbarycentric=np.zeros((len(G),2))
Gbarycentric[:,0]=G[:,0]*a[0,0]+G[:,1]*b[0,0]+G[:,2]*c[0,0]
Gbarycentric[:,1]=G[:,0]*a[0,1]+G[:,1]*b[0,1]+G[:,2]*c[0,1]
ax.scatter(Gbarycentric[:,0], Gbarycentric[:,1], dobs, c='c', marker="v", alpha=1)


tmp=np.concatenate((mopt[:,0],[mopt[0,0]]),axis=0)
ax.text(a[0,0],a[0,1]+0.1,mopt[0,0], name[0],color="red")
ax.text(b[0,0],b[0,1],mopt[1,0], name[1],color="blue" )
ax.text(c[0,0],c[0,1],mopt[2,0]+0.4, name[2],color="green" )
ax.plot(summits[:,0],summits[:,1],tmp,color='c' )
ax.view_init(elev=12, azim=53)
plt.savefig('image_3dview.eps', format='eps')


#############################
# OUTPUT 2D IMAGE  
#############################
plt.figure(2)
#print(np.shape(stddobs),np.shape(stdcalens))

plt.xlabel('$\u03B4^{202}$Hg(\u2030)_exp')
plt.ylabel('$\u03B4^{202}$Hg(\u2030)_cal')
plt.title('calculated versus measured species-averaged values of $\u03B4^{202}$Hg\n, and associated linear fit')
plt.scatter(dobs, dcal,marker='o',label='calculated/measured species-averaged values')
xmin=np.amin(dobs)
xmax=np.amax(dobs)
ymin=linfit[0]*xmin+linfit[1]
ymax=linfit[0]*xmax+linfit[1]
#print('y='str(linfit[0,0])'*x +'str(linfit[1,0]))
plt.plot([xmin,xmax],[ymin,ymax],label='y='+str(np.around(linfit[0,0],4))+'*x +'+str(np.around(linfit[1,0],4))+' with R2='+str(np.around(R2,4)))#'y='str(linfit[0]))#'*x+'str(linfit[1]))
plt.savefig('image_2dfit.eps', format='eps')

plt.legend(loc='best')


plt.show()



