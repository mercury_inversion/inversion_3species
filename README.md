# inversion 3 species

## presentation

The Python code of the Inv3-202Hg software calculates the isotopic ratio of 202Hg to 198Hg (d202Hg) of three Hg species from the knowledge of their fractional amounts with uncertainties, as determined for example by HR-XANES spectroscopy, and of the total (i.e., species-averaged) ratios of 202Hg to 198Hg with their uncertainties, as determined by isotopic analysis.

Authors: Romain Brossier (romain.brossier@univ-grenoble-alpes.fr) Alain Manceau (alain.manceau@univ-grenoble-alpes.fr)

This code is associated with the article Manceau, Alain; Brossier, Romain; Janssen, Sarah; Rosera, Tylor; Krabbenhoft, David; Cherel, Yves; Bustamante, Paco; Poulin, Brett (2021) "Mercury Isotope Fractionation by Internal Demethylation and Biomineralization Reactions in Seabirds: Implications for Environmental Mercury Science", Environ. Sci. Technol., submitted


---------------------------------
## inputs

The code requires two files, a dataset in .csv format and an input file in ascii which invokes Inv3-202Hg. The two files used in the Manceau et al. (2021) article are provided in the Git repository.

### the general structure of the csv file is as follows

header line 1

.....

header line n

sample ID1 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; Std %MeHg ; %Hg(Sec)4 ; Std %Hg(Sec)4 ; %HgSe ; Std %HgSe 

sample ID2 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; Std %MeHg ; %Hg(Sec)4 ; Std %Hg(Sec)4 ; %HgSe ; Std %HgSe

.....

sample IDm ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; Std %MeHg ; %Hg(Sec)4 ; Std %Hg(Sec)4 ; %HgSe ; Std %HgSe

where for each sample

- "sample ID" and "sample name or description" are text strings to identify the sample (just for display purpose)
- "d202Hg" and "Std d202Hg" are floating numbers related to the measured value of d202Hg and standard deviation for the sample
- "%MeHg", "%Hg(Sec)4" and "%HgSe" are floating numbers related to the percentage of MeHg, Hg(Sec)4 and  HgSe in the sample, respectively
- "Std %MeH", "Std %Hg(Sec)4" and "Std %HgSe" are floating numbers related to the  standard deviation of the percentage of MeHg, Hg(Sec)4 and  HgSe in the sample, respectively

The format follows the .csv standard with ";", commented lines in the data file start with a "#" and are ignored at the reading time

### the general structure of the input file is as follows

1. the name of the .csv file, including its extention
2. the number of header lines (n, integer)
3. the number of random samplings for the statistical analysis

In addition to providing an output log, the software creates
1. a 3D image of the f(MeHg), f(Hg(Sec)4), f(HgSe) hyperplane and experimental d202Hg values (vector postscript image_3dview.eps in .eps format),
2. a 2D image of the calculated versus measured species-averaged values of d202Hg and calculates the linear fit. (vector postscript image_2dfit.eps in .eps format)


---------------------------------------

## running examples

1. To reproduce the Manceau et al. (2021) results launch `python3 Inv3-202Hg.py < petrel_input.txt` or `python Inv3-202Hg.py < petrel_input.txt` (if the default python calls python3.X)

For running the examples while directing the output to a log text file, you can launch
1. `python3 Inv3-202Hg.py < petrel_input.txt > log_out_petrel.txt` or  `python Inv3-202Hg.py < petrel_input.txt > log_out_petrel.txt`

